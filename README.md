# cps-informel-200924

Présentation du SSP Cloud aux chef.fe.s de SSM (24 septembre 2020)

## Contexte

Présentation du projet DDODS un an auparavant (24 septembre 2019), voir [pdf](./amont/3_DDODS.pdf)

**Résumé :** 

- besoin (2017) : une infrastructure en libre service sur des technologies de datascience et ingénierie logicielle

- cloud privé orienté traitement de la donnée

- objectif d'ouverture au SSP à fin 2019

- presta d'AMOA

# TODO

- [ ] note préparatoire : que sera le SSP Cloud ? synthétiser l'offre, les cas d'usage et les grands principes de gouvernance

- [ ] diapo de présentation (2O minutes + 15 minutes de Q/R)
  * faire un point sur les travaux depuis 1 an (presta AMOA, hébergement au White, achat de matériel, beta Spyrales, etc.)

