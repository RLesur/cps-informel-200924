---
title: SSP Cloud
subtitle: Un environnement mutualisé d'expérimentations pour la statistique publique
lang: fr
paged-footnotes: true
output: 
  pagedown::html_paged:
    css: static/note.css
    number_sections: false
    self_contained: false
    includes:
      in_header: fix_instances.html
knit: pagedown::chrome_print
---

::: suivi

Dossier suivi par :\
Arnaud Degorre\
Romain Lesur

:::

::: logo
![](./static/insee.png) ![](./static/logo-statpub.png)
:::

L'Insee va prochainement mettre à disposition des acteurs de la statistique
publique une infrastructure expérimentale nommée "SSP Cloud".

Il s'agit d'une plateforme de services dédiés aux expérimentations statistiques 
et à la datascience utilisant les technologies _open source_ les plus récentes, 
réunies sous la forme d'un _datalab_.  Cette infrastructure se fixe pour ambition
de permettre aux projets de la statistique publique d'expérimenter aisément
de nouvelles méthodes de traitement, y compris sur données volumineuses. 
Parce que ces expérimentations nécessitent de partager les savoirs et savoir-faire, 
et qu'elles sont source de synergie entre l'ensemble des acteurs de la statistique publique,
le SSP Cloud est conçu sous la forme d'un service mutualisé, ouvert à une communauté d'utilisateurs.
Ces derniers bénéficieront ainsi d'environnements de travail (RStudio, JupyterLab...), d'un environnement de
stockage des données, d'un service de gestion de la sécurité et d'un dispositif d'entraide et d'échange. 


![](./static/services.jpg){style="margin-left:20%; width: 60%; border-radius: 10px;"}

Évolutive par essence, la plateforme d'expérimentation SSP Cloud couvre un ensemble d'usages possibles, amenés à être enrichis en fonction des besoins du SSP et de l'expérience des utilisateurs. Les services intégrés au SSP Cloud permettent en particulier :

- de partager des éléments de connaissance et de déployer des ressources pédagogiques et des formations en ligne ;
- de prototyper des traitements statistiques et de réaliser des projets d'études mobilisant des données ouvertes, en s'appuyant sur des ressources logicielles et matérielles à l'état de l'art ;
- d'engager des projets partenariaux facilitant le travail collaboratif d'équipes relevant de différents organismes ;
- de conduire, sous réserve des avis d'opportunité et de sécurité requis, des expérimentations sécurisées sur des données confidentielles.


## La poursuite d'un investissement engagé en 2019

La mise à disposition du SSP Cloud représente une étape de plus de franchie
dans le cadre du projet DDODS (Découverte Démultipliée des Outils de Data Science), engagé début 2019. Ce projet, porté à l'Insee par la Division Innovation
et instruction technique (Diit) au sein de l'Unité Innovation et stratégie
du SI (Unissi), a fait l'objet de présentations sur les objectifs 
poursuivis et les attentes du SSP à l'occasion du GitiSSP (12\ février et 19\ juin\ 2019)
et du CPS informel du 24\ septembre\ 2019. 

Le projet a bénéficié d'un financement par le Fonds de transformation
ministériel du MEF, à hauteur de 200\ k€ sur l'exercice 2019. Cet appui a permis 
de faire l’acquisition de prestations d’accompagnement (ingénierie, choix d’hébergement, 
gouvernance) et du matériel idoine (renouvellement de serveurs). 
Lauréat d'un appel à candidatures d'Etalab, le projet s'appuie également sur la
mobilisation d'entrepreneurs d'intérêt général (EIG).
Ces EIG, présents pour 10 mois, apporteront leurs compétences en
design, développement et ingénierie des données. Leur présence représente une
opportunité afin de recueillir les besoins,
améliorer l'expérience utilisateur et enrichir les services. 

Après avoir prototypé la plateforme sous la forme d'une première instance ouverte
auprès des agents de l'Insee, une phase de test a été conduite au printemps 2020,
en ouvrant une instance temporaire du datalab pour la démarche Spyrales,
une communauté d'agents publics (avec une majorité d'agents des SSM) 
désireux de s'entraider dans l'apprentissage des langages R et python.
Ces expériences préalables permettent désormais d'ouvrir le SSP Cloud,
sous la forme d'un service pérenne^[Par pérennité, il faut entendre que le service est mis en place sans date de fin préétablie. La longévité du service reste conditionnée à l'utilisation effective du service proposé et à la capacité de mobiliser les ressources humaines et techniques requises dans la durée.] accessible aux acteurs et aux projets 
de la statistique publique.

## Un projet _open source_ et des instanciations

Le projet DDODS comprend deux composantes bien distinctes :

- un projet logiciel _open source_ nommé Onyxia, maintenu par la Diit^[Le projet Onyxia comprend [une partie serveur](https://github.com/InseeFrLab/onyxia-api), [une partie client](https://github.com/InseeFrLab/onyxia-ui) ainsi qu'[un catalogue de services de datascience](https://github.com/InseeFrLab/Universe-Datascience).]. Le logiciel Onyxia permet, une fois déployé, de disposer d'un espace depuis lequel le statisticien accède à des services de datascience (par exemple, RStudio), crée des environnements de travail sur mesure, lance des formations interactives, gère les espaces de stockage associés aux projets... 
- des instances d'orchestration, qui correspondent à la mise en œuvre du dispositif pour un ensemble d'utilisateurs donné, avec les ressources physiques (serveurs...) indispensables à l'exécution du projet Onyxia, les conditions d'usage correspondantes, les principes de gouvernance et les moyens associés de maîtrise d'œuvre. Nous appelons ainsi "instance" un dispositif technique et organisationnel permettant d'exécuter le projet Onyxia pour un ensemble donné d'utilisateurs et d'usages. 

![](./static/instances.png){id="instances" style="width: 73%; border-radius: 10px;"}

À ce jour, il existe une instance pérenne d'Onyxia accessible uniquement en interne à l'Insee. Celle-ci est administrée par la Diit et peut être utilisée par tout
agent de l'Insee qui le souhaite.

Durant le confinement, une instance a été temporairement mise à disposition de
la communauté Spyrales par l'Insee afin
que cette communauté d'agents publics puisse se former à R et python.

Le _SSP Cloud_ représentera donc la deuxième instance pérenne du projet
Onyxia. Elle a vocation a être administrée par l'Insee et des représentants des
SSM utilisateurs de cette instance. Elle sera dédiée à la communauté des statisticiens publics,
avec les cas d'usages qui seront retenus par sa gouvernance.

N'importe quel acteur disposant des ressources organisationnelles et techniques adaptées peut créer sa
propre instance d'Onyxia. En effet, le projet logiciel Onyxia est développé en _open source_ 
afin qu'il puisse être installé par qui le souhaite^[Contrairement à l'utilisation d'outils propriétaires tels que SAS, l'utilisation de logiciels _open source_ offre une portabilité de ces travaux dans d'autres environnements d'exécution, sans être exposé aux risques posés par les politiques de licensing des éditeurs. Un SSM pourra ainsi, s'il le souhaite, recréer son propre datalab sur ses serveurs internes.] d'une part et d'autre part, de recueillir des
contributions extérieures pour enrichir les possibilités fonctionnelles du logiciel. 

Chaque instance a sa communauté propre d'utilisateurs -- ce sera donc le cas pour le SSP Cloud, 
pour être au plus près des besoins et attentes de la statistique publique. 
Par construction, la communauté d'utilisateurs d'Onyxia comprend 
l'ensemble des utilisateurs des différentes instances exécutant le projet,
et cela afin de pouvoir bénéficier de la plus grande audience possible dans le monde de la datascience.


## Explorer de nouvelles possibilités en datascience : les cas d'usage du SSP Cloud

Le datalab SSP Cloud sera accessible sur internet à l'adresse
[datalab.sspcloud.fr](http://datalab.sspcloud.fr) et est physiquement hébergé à l'Insee. Il se
prête notamment aux nouvelles conditions de travail que peuvent connaître les
agents dans le contexte épidémique actuel. Souple et évolutive, cette
plateforme apporte des aménités facilitant le travail collaboratif en matière de datascience.

Elle a vocation à s'adapter aux besoins de sa communauté d'utilisateurs, du
débutant en R ou python, jusqu'à l'expert en _deep learning_ ayant besoin de
ressources spécifiques, telles que des cartes graphiques. Le code source de la
plateforme étant ouvert, les utilisateurs les plus avancés pourront, s'ils le
souhaitent, contribuer à l'évolution des services offerts.

Une des vocations de cette plateforme est également de favoriser l'adoption des
bonnes pratiques en matière de statistique reproductible. Les retours
d'expérience des utilisateurs ont vocation à faire évoluer le projet afin de
renforcer l'appropriation de ces méthodes de travail.

Le SSP Cloud pourra se prêter aux usages suivants :

- disposer d'un environnement intégré de formation et d'apprentissage en ligne
dans le domaine de la datascience. L'instance temporaire utilisée durant le
confinement a permis à des formateurs de déployer des formations pouvant être
utilisées de façon synchrone lors d'une session de formation ou d'un webinaire
ou bien de façon asynchrone pour de l'auto-formation.
- partager des ressources techniques (documentation, méthodes de traitement,
exemples appliqués) et les publier (wiki, site documentaire).
- permettre à des équipes de prototyper des pipelines de traitement
statistique mobilisant des ressources avancées, logicielles (telles que Spark)
ou matérielles (cartes graphiques) avec la possibilité de dimensionner ces
ressources.
- permettre à des équipes multi-institutionnelles de travailler ensemble, en
créant des environnements collaboratifs sécurisés. Exemples : projets
impliquant l'Insee et des SSM, des membres du SSP ou du système statistique européen,
et des partenaires extérieurs (chercheurs, partenaires étrangers, prestataires, entreprises...).
- d'autres usages à découvrir selon les attentes des utilisateurs -- et à valider par la gouvernance du SSP Cloud.

La principale limite du SSP Cloud est qu'il ne pourra pas être utilisé pour
exécuter des chaînes de production statistique. En effet, le SSP Cloud
reste une instance expérimentale avec un niveau d'engagement de service de type
_best effort_ (il n'y a pas de garantie de disponibilité). Plus généralement, les travaux présentant
une criticité élevée pour l'institution utilisatrice (par exemple, la production
d'indicateurs inscrits dans un calendrier officiel de publication) n'ont pas vocation
à être conduits sur la plateforme SSP Cloud, de même que les travaux "récurrents"
qui ne relèveraient plus d'une logique de prototypage et d'expérimentation.


## Le SSP Cloud et la sécurité

### Principes généraux

Le SSP Cloud fera l'objet d'une démarche de qualification auprès de la
commission d'homologation de l'Insee. L'engagement d'un audit de sécurité par
l'Anssi est également inscrit dans la feuille de route du projet.

Par ailleurs, la sécurité est assurée par un ensemble de mesures techniques,
mais aussi de mesures organisationnelles.

L'utilisation du SSP Cloud sera conditionnée à la validation, par chaque
utilisateur, des conditions générales d'usage, également reprises
dans une charte signée par l'ensemble des SSM adhérents au dispositif. Chaque
SSM utilisateur aura, en amont, signé une convention bilatérale avec l'Insee
relative aux engagements réciproques.

Le SSP Cloud est fondé sur des principes de _Privacy by Design_. Le
cloisonnement des environnements de travail de chaque utilisateur ainsi que la
possibilité de chiffrer de bout en bout les données ont été pris en compte dès
la conception du datalab. Pour permettre le travail à plusieurs, un système de
gestion de groupe est possible : les personnes membres d’un groupe pourront
partager des ressources, dont notamment des informations stockées.

Le stockage de données disponibles en _open data_ sera autorisé par défaut.

### Cas particulier des projets d'expérimentation sur données confidentielles

L'accueil de données confidentielles nécessitera systématiquement et au
préalable, la validation des autorités qualifiées pour la sécurité des systèmes
d'information (AQSSI) du système d'information d'origine et de l'Insee sur la
base d'une analyse de risques impliquant l'ensemble des acteurs concernés.

Cet accueil sera par ailleurs accordé pour un projet d'expérimentation donné :
son opportunité sera préalablement appréciée et les usages des
données confidentielles seront donc strictement encadrés et limités aux
contours du projet d'expérimentation ayant fait l'objet d'un visa. Le partage
de données chiffrées ne sera possible que sur la base d'une action volontaire
d'un utilisateur habilité et dans la limite des accès prévus dans l'avis de
sécurité associé à l'expérimentation concernée.

## Gouvernance, maîtrise d'ouvrage et maîtrise d'œuvre

La gouvernance du SSP Cloud sera placée sous l'autorité d'un comité stratégique
rassemblant les représentants de l'Insee et des représentants des SSM
utilisateurs. Il se prononcera sur :

- les cas d'usage habilités de la plateforme ;
- le périmètre d'ouverture des utilisateurs de la plateforme (agents des SSM,
autres services émargeant à des opérations de statistique publique, partenaires
académiques...) ;
- l'accueil de projets d'expérimentation lorsque ces derniers portent sur des
données relevant de critères "Haute Protection" (données individuelles, données
privées relevant du secret commercial) ;
- la priorisation des demandes d'évolutions notamment matérielles, de
l'instance SSP Cloud ;
- les contributions de l'Insee et du SSP pour améliorer l'offre de services du
SSP Cloud (humaine, technique ou financière).

La composition du comité stratégique sera établie sur la base d'un appel à
participation, transmis au 4^ème^\ trimestre 2020 à l'ensemble des chefs de SSM,
dans la perspective de mettre en place le comité pour janvier 2021. Elle a
vocation à réunir des acteurs à des niveaux décisionnaires, légitimes par leur
fonction ou sur délégation pour engager l'utilisation de l'instance SSP Cloud
par les agents relevant du SSM concerné. Concernant l'Insee, le comité
stratégique associera des représentants de la DSI et de la DMCSI. Le comité
stratégique se réunira deux fois l'an.

La maîtrise d'ouvrage du SSP Cloud est assurée par l'Unité Innovation et
stratégie du SI (Insee - DSI) et le SSP Lab (Insee - DMCSI). Par ailleurs, l'Unissi met
en œuvre les moyens humains, techniques et financiers appropriés pour répondre
aux utilisations et aux orientations du SSP Cloud, telles que définies par le
comité stratégique. Ces moyens sont discutés dans le cadre d'un dialogue de
gestion avec la DSI, la DMCSI et le secrétariat général de l'Insee. Ils
s'appuient, au démarrage du SSP Cloud, sur les contributions de l'Insee. 
Ils pourront, le cas échéant et dans le cadre des arbitrages retenus par le comité stratégique, être abondés
par des ressources partagées dans le réseau SSP (contributions humaines,
techniques ou financières).

Tous les utilisateurs seront conviés à participer à la communauté
d'utilisateurs du SSP Cloud d'ores et déjà créée sur Tchap.^[[Salon **SSPCloud**](https://www.tchap.gouv.fr/#/room/#SSPCloudXDpAw6v:agent.finances.tchap.gouv.fr), les agents potentiellement intéressés peuvent rejoindre dès maintenant cette communauté.] Cette communauté d'utilisateurs vise à

- améliorer l'offre d'assistance et l'aide à l'utilisation des services du SSP Cloud
- documenter les principaux problèmes qui émergeraient dans l'offre du SSP Cloud
et ses usages
- formuler les évolutions fonctionnelles souhaitées.

Le bureau du SSP Cloud sera chargé du suivi opérationnel de la plateforme. À
cette fin, il aura la responsabilité :

- d'établir un suivi des usages de plateforme et de faire un bilan semestriel
présenté au comité stratégique ;
- de prendre part à l'animation et la modération de la communauté
d'utilisateurs ;
- d'assurer les échanges avec les équipes techniques en charge de maîtrise
d'œuvre du SSP Cloud ;
- d'assurer les échanges avec le _Board_ du projet _open source_ Onyxia ;
- de conduire l'instruction pour les demandes d'accueil de projets
d'expérimentation en relation avec les AQSSI / RSSI concernés.

Distincte de la gouvernance du SSP Cloud, la gouvernance du projet _open source_ Onyxia s'alimente des demandes priorisées d'évolution formulées par
chaque "instance" utilisatrice -- le SSP Cloud, mais aussi chaque SSM ou autres
structures émargeant à la statistique publique qui aura créé son propre centre
de traitement en se fondant sur Onyxia. À sa mise en place, cette gouvernance
sera organisée autour d'un _board_ associant les principaux contributeurs au
projet _open source_, d'un représentant de l'instance déployée à l'Insee et
d'un représentant du bureau du SSP Cloud (composition susceptible d'évoluer
selon les autres adoptions du dispositif à l'avenir).

## Une ouverture en deux phases

L'ouverture du SSP Cloud sera conduite en deux phases en lien avec la présence
des entrepreneurs d'intérêt général. Les compétences nouvelles apportées par
les EIG notamment en design, représentent une opportunité afin d'améliorer
l'expérience utilisateur de la plateforme.

La première phase sera donc une phase d'accueil, de découverte et d'écoute de
l'expérience utilisateur durant laquelle les EIG seront présents. Cette phase
s'ouvrira le 5 octobre 2020. Les SSM peuvent manifester leur intérêt en
adressant un mail à <innovation@insee.fr>.  Les agents seront invités à
rejoindre la communauté Tchap
[_SSPCloud_](https://www.tchap.gouv.fr/#/room/#SSPCloudXDpAw6v:agent.finances.tchap.gouv.fr).

Durant cette première phase, les utilisateurs seront également invités à
participer, sur la base du volontariat, à des moments d'échanges avec l'EIG
designer portant sur l'expérience utilisateur à des fins d'amélioration. Cette
phase pourra aussi être l'occasion, pour les SSM, d'évaluer la plateforme.

![](./static/ouverture.png){style="border-radius: 10px;"}

La seconde phase sera une phase d'enrôlement (2^ème^ trimestre 2021), avec
création des accès pour l'ensemble des agents habilités par les SSM
partenaires.

L'ouverture de la seconde phase sera conditionnée à la signature préalable de
la convention régissant les engagements réciproques de l'Insee et du SSM
utilisateur.
