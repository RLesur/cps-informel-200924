<!-- .slide: data-background-image="./images/goals.jpg" -->
# L'intention <!-- .element: style="color:#ededed;margin-top: -11%;padding-right:45%;" -->

---

## Les objectifs

- Fournir à la statistique publique une plateforme collaborative de  
  _data science_

- Lieu de découverte et d'expérimentation

- Technologies les plus récentes
  * _open source_
  * _cloud computing_
  * _big data_
  * calculs intensifs
  * sécurité

---

<!-- .slide: data-background-image="./images/fonctionnalites.png" data-background-size="contain"-->

---

## Les solutions proposées

<div class="r-stretch">
<img style="height:90%;" src="./images/services.png">
</div>

---

## Des usages démultipliés

- Formation et apprentissage en ligne
- Partage de ressources techniques
- Prototypage de traitement de _big data_
- Entraînement de modèles de _machine learning_
- Collaboration entre institutions
- Autres usages...

---

## Des moyens mis à disposition

<div class="r-stretch">
<div style="display: grid; grid-template-columns: 35% 42.5%;grid-template-rows: 50% 50%; margin-left: 20%;">
<img style="border-radius:5px;" src="./images/machine1.jpg">
<img style="border-radius:5px;grid-column:2;grid-row:1 / span 2;" src="./images/machine2.jpg">
<img style="border-radius:5px;" src="./images/machine3.jpg">
</div>
</div>

---

## Sous une forme encore expérimentale

<div class="r-stretch">
<img style="float: left;" src="./images/machine4.jpg">

<div style="height: 20%">
</div>

- Niveau d'engagement de service de type  
  _best effort_
- Ne peut accueillir des chaînes de production
- Prototypage et expérimentation seulement
</div>

---

<!-- .slide: data-background-image="./images/saisons.jpg" -->
# Un projet en plusieurs saisons <!-- .element: style="color:#5dcdad;text-shadow: 4px 4px 0 rgba(53, 57, 63, 0.9)" -->

---

## La poursuite d'un investissement<br/>engagé en 2019

- Projet DDODS soutenu par
  * FTM du MEF (200 k€ en 2019)
  * Etalab (3 EIG 2020/2021)

- Deux prototypes réalisés
  * interne à l'Insee
  * test d'une plateforme accessible depuis internet (Spyrales)

---

## Un test concluant : l'ouverture à Spyrales

<div class="r-stretch">
<img style="border-radius:5px; height:90%;" src="./images/formations.png">
</div>

---

## Un projet open source<br/>Des instanciations

- Une application (Onyxia) développée en _open source_  
  <https://github.com/InseeFrLab>
  
- Installée sur différents environnements (instances)  

- Chaque instance définit sa propre gouvernance,  
  ses propres règles d'usage...

---

<!-- .slide: data-background-image="./images/instances.png" data-background-size="contain"-->

---

## Les contributions sont bienvenues

<div class="r-stretch">
<img style="border-radius:5px;height: 90%;" src="./images/gh.png">
</div>

---

<!-- .slide: data-background-image="./images/sunrise.jpg" -->
# Une ouverture maîtrisée <!-- .element: style="color:#e17729;margin-top:30%;" -->

---

## Le SSP Cloud et la sécurité

- Cloisonnement des environnements par défaut  
  _privacy by design_
- Possibilité de chiffrement des données de bout en bout
- Données ouvertes autorisées par défaut
<p></p>
<b>Objectifs :</b> 
- Démarche de qualification auprès de la commission d'homologation de l'Insee
- Audit de sécurité Anssi

---

## Les projets d'expérimentation<br/>sur données sensibles

Accueil de données sensibles : <!-- .element: style="text-align: left; margin-left: 10%;" -->

- limité à un projet d'expérimentation donné  
  _analyse de l'opportunité_
- soumis à la validation des AQSSI du SI d'origine et de l'Insee  
  _analyse de risques_
  
---

## Un bien mutualisé<br/>sous gouvernance du SSP...

<div>

- Comité stratégique du SSP Cloud<br/>
- Bureau du SSP Cloud

</div>

---

## ...amorcé par l'Insee

<div>

- MOA Insee : Unissi/SSP Lab

- Moyens humains : Diit (4 agents)  
  Renfort de 3 EIG (Etalab)

- Moyens financiers : Insee  
  Soutien du MEF en 2019

</div>

---

<!-- .slide: data-background-image="./images/ouverture.png" data-background-size="contain" -->


---

### Une démarche d'écoute des utilisateurs <!-- .element: style="margin-top: -27%;" -->

<!-- .slide: data-background-image="./images/utilisateurs.png" data-background-size="contain" -->

---

## En résumé

Le SSP Cloud <!-- .element: style="text-align: left; margin-left: 10%;" -->

- un espace d'expérimentation prêt à accueillir les équipes du SSP
- sous une gouvernance commune du SSP
- et une offre à façonner au plus près des cas d'usage

---

## Quand ?

**le 5 octobre 2020**  
[datalab.sspcloud.fr](http://datalab.sspcloud.fr)

et dès maintenant  
[![](./images/tchap.png)](https://www.tchap.gouv.fr/#/room/#SSPCloudXDpAw6v:agent.finances.tchap.gouv.fr) <a href="https://github.com/InseeFrLab"><img src="./images/logo.png" style="border-radius:12px;"></a>

<innovation@insee.fr>

---

## Merci de votre attention
