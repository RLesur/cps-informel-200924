Proposition de trame, à amender allègrement.

# Slide d'ouverture

SSP Cloud
Un environnement mutualisé d’expérimentations pour la statistique publique

# L'intention du projet

## Les objectifs poursuivis 

Une reformulation des messages ci-dessous

Il s’agit d’une plateforme de services dédiés aux expérimentations statistiques et à la datascience utilisant
les technologies open source les plus récentes, réunies sous la forme d’un datalab. Cette infrastructure se
fixe pour ambition de permettre aux projets de la statistique publique d’expérimenter aisément de nouvelles
méthodes de traitement, y compris sur données volumineuses. Parce que ces expérimentations nécessitent
de partager les savoirs et savoir-faire, et qu’elles sont source de synergie entre l’ensemble des acteurs de la
statistique publique, le SSP Cloud est conçu sous la forme d’un service mutualisé, ouvert à une communauté
d’utilisateurs.

## Le besoin fonctionnel

La diapo "Les principales fonctionnalités" Figma, en enlevant la mention des logiciels

##  Les solutions proposées...

Image 1 de la note de synthèse -> permet d'expliquer que ces besoins sont couverts par l'association d'un ensemble de logiciels open source, dont la composition et l'exécution est gérée (pour simplifier et du point de vue de l'utilisateur) par Onyxia

## ... et des usages démultipliés...

Reformuler la page 3 de la note de synthèse et les items listés, pour terminer cette première partie sur une illustration assez appliquée des usages retenus.

## ... en y mettant les moyens

Photo des serveurs dans la salle machine + photo des cartes graphiques empilées

# Un projet en plusieurs saisons

## La poursuite d'un investissement engagé en 2019

Reformuler la partie éponyme de la note de synthèse p2

## Un test concluant : l'ouverture à Spyrales

Le présenter positivement à l'oral (200 personnes s'y sont essayées...). 

En support, je propose la capture d'écran de l'onglet "Formations" où l'on voit qq parcours (en plus ça fera de la pub pour le funcamp :-)


## Un projet Open Source et des instanciations (1)

Deux composantes bien distinctes :
- un projet logiciel open source nommé Onyxia. Le logiciel Onyxia permet, une fois
déployé, de disposer d’un espace depuis lequel le statisticien accède à des services de datascience, crée des environnements de travail sur mesure, lance des formations interactives, gère les espaces de stockage associés aux projets…
- des instances d’orchestration, qui correspondent à la mise en oeuvre du dispositif pour un ensemble
d’utilisateurs donné, avec les ressources physiques indispensables à l’exécution du projet
Onyxia, les conditions d’usage correspondantes, les principes de gouvernance et les moyens associés
de maîtrise d’oeuvre. 

## Un projet Open Source et des instanciations (2)

On précise et clarifie la notion d'instance via l'image 2 de la note de synthèse

## Tous peuvent contribuer

Capture d'écran du dépôt onyxia-ui et universe-datascience sur le compte InseeFrLab

# Une ouverture maîtrisée

## Le SSPCloud et la sécurité

Cf. principes généraux de la note de synthèse

## Les projets d'expérimentation sur données sensibles

cf. la partie éponyme de la note de synthèse

## Un bien mutualisé, sous votre gouvernance...

- Comité stratégique
- Bureau du SSP

## ... et l'amorçage par l'Insee

A reformuler 
La maîtrise d’ouvrage du SSP Cloud est assurée par l’Unité Innovation et stratégie du SI (Insee - DSI) et le
SSP Lab (Insee - DMCSI). Par ailleurs, l’Unissi met en oeuvre les moyens humains, techniques et financiers
appropriés pour répondre aux utilisations et aux orientations du SSP Cloud, telles que définies par le comité
stratégique. Ces moyens sont discutés dans le cadre d’un dialogue de gestion avec la DSI, la DMCSI et le
secrétariat général de l’Insee. Ils s’appuient, au démarrage du SSP Cloud, sur les contributions de l’Insee. Ils
pourront, le cas échéant et dans le cadre des arbitrages retenus par le comité stratégique, être abondés par
des ressources partagées dans le réseau SSP (contributions humaines, techniques ou financières).

## Une ouverture séquencée, et néanmoins immédiate

Image 3 de la note de synthèse

## Une démarche d'écoute des utilisateurs

Peut-être avec l'image Figma "Catégories d'utilisateurs" ?

# We need you
le SSPCloud,
- un espace d'expérimentation prêt à accueillir vos équipes
- sous une gouvernance commune du SSP
- et une offre à façonner au plus près des cas d'usage

